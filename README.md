<h4> 1 feladat.</h4>
Orai: Generaljon egy szamot 1-1000 kozott.
A felhasznalotol kerjen be egy szamot a konzolrol, ha  a bekert szam kisebb, mint a random szam, akkor  irja ki, hogy kisebb, ha nagyobb akkor irja ki, hogy nagyobb. Ha egyenlo, akkor veget er a jatek, irja ki, hogy hany probalkozasbol sikerult eltalalni a felhasznalonak.

<h4>Hazi feladat:</h4>
1. Kerdezze meg a felhasznalot, hogy szeretne e ujra jatszani.
2. A jatek elejen kerdezze meg a jatekost hogy milyen intervallumba generaljon egy szamot. (1- X)
3. A jatek elejen kerjen be egy becenevet a jatekostol.
4. Mentse el a jatekos eredmenyeit egy fajlba. A fajlt egy elore fixen meghatarozott konyvtarba mentse el. A fajl neve legyen a jatekos beceneve txt kiterjesztessel. pl: c:\guessanumber\player1.txt
A fajlba mentse el az aktualis datumot, a gondolt szamot, es a lepesek szamat.
5. Keszitsen egy konzolos menut amin keresztul a felhasznalonak lehetosege van
    1. Uj felhasznalo nevet megadni
    2. Uj jatekot inditani.
    3. Sajat korabbi eredmenyeket visszanezni
    4. Sajat Top3 eredmenyt megnezni
    5. Sajat korabbi eredmenyeket torolni
    6. Kilepni a jatekbol
6. Egeszitse ki a menut, hogy a tobbi jatekos nevet is listazza ki.
7. Egeszitse ki a menut, hogy mas jatekos eredmenyeit is meg lehessen tekinteni, ehhez a jatekosnak meg kell adnia a masik jatekos nevet.
8. Egeszitse ki a menut, hogy mas jatekos top3 eredmenyeit is meg lehessen tekinteni, ehhez a jatekosnak meg kell adnia a masik jatekos nevet.
9. Egeszitse ki a menut, hogy lehetoseg legyen minden jatekos eredmenyeibol osszeallitot Top10-es listat megtekinteni.

<h4>2 feladat.</h4>
Orai: Keszitsen egy menus alkalmazast, az alabbi menupontokkal:
Aktulis fajl:
0. Kilepes
1. Uj fajl megadasa
2. Fajl adatok kiirsa
3. Fajl tartalmanak kiirsa

Aktualis fajl utan irja ki, hogy van e megadott munkafajl, ha nincs, akkor irja ki, hogy nincs aktualis fajl.
Kilepes hatasara lepjen ki a programbol.
Uj fajl menupont hatasara adjon meg uj munkafajlt.
Fajl adatok kiirasa menupont hatasara irja ki a fajl adatait.
  Letezik e? Fajl vary konyvtar? Irate e? Mekkora a merete? Mi az utolso modositas datuma?
Fajl tartalmanak kiirasa menupont hatasara irja ki a fajl tartalmat.
