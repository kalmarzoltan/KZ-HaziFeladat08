package hazi_8.feladat_1;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class NumberGuess {


   static String answer = "y";

    static Scanner scn = null;
    static Scanner scn2 = null;
    static String nickName = null;
    static int tries = 0;
    static int gamesNo = 0;
    static String eredmény = null;
    static int number;
    static StringBuilder sb = new StringBuilder();
    static List<Integer> numbers = new ArrayList<>();


    public NumberGuess() {

    }

    public NumberGuess(Gamer gamer) {
        nickName=gamer.getName();
    }

    public void guessTheNumber() {

        System.out.println("Enter Nickname: ");
        Scanner scn3 = new Scanner(System.in);
        nickName = scn3.nextLine();

        if(nickName.equals("")){
            System.err.println("Nem adtál meg nevet");
        }else {

            System.out.println("Hi " + nickName + " lets play!");
            Date date = new Date();
            Gamer gamer = new Gamer(nickName,gamesNo,tries,numbers);


            while (answer.equalsIgnoreCase("y")) {
                gamesNo++;
                jatek();
            }

            gamer.setNumberOfGames(gamesNo);
            gamer.setNumberOfTries(tries);
            gamer.setGondoltSzamok(numbers);

//            eredmény = "Dátum: " + new SimpleDateFormat("yyyy-MM-dd").format(date)
//                    + ", Játékos: " + nickName
//                    + "; játékok száma: " + gamesNo
//                    + ", összes próbálkozások száma: " + tries
//                    + ", a gondolt szám(ok): ";

            eredmény = gamer.toString();

            sb.append(eredmény);
            sb.append(numbers.toString());
            System.out.println("Kösz a játékot " + nickName + " , összesen ennyiszer próbálkoztál: " + tries);




            String fileName = "C:\\Users\\Kalmi\\Documents\\gamers.txt";


            try {


                BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, true));
                bw.write(eredmény + "\n");
                bw.close();

            } catch (FileNotFoundException e1) {
                System.out.println("FileNotFoundException van");
            } catch (IOException e2) {
                System.out.println("IOException van");
            }
        }
    }


    public static void jatek() {

        System.out.println("Mennyi legyen a maximum szám?: ");

        try {
            scn = new Scanner(System.in);
            int max;
            max = scn.nextInt();

            Random rand = new Random();
            number = rand.nextInt(max) + 1;

            System.out.println(number); //ezt kell kitalálni

            scn2 = new Scanner(System.in);
            int guess;
            boolean win = false;


            while (win == false) {
                System.out.println("Írj be egy számot  1 és " + max + " között: ");
                guess = scn2.nextInt();
                tries++;

                if (guess == number) {
                    win = true;
                } else if (guess < number) {
                    System.out.println("Túl alacsony, próbáld újra");
                } else if (guess > number) {
                    System.out.println("Túl magas, próbáld újra");
                }
            }
            System.out.println("Talált");
            numbers.add(number);
            System.out.println(nickName + " próbálkozások száma: " + tries);




            System.out.println("Új játék? Y/N");


            answer = scn.next();


        } catch (InputMismatchException ex) {
            System.out.println("Nem számot adtál meg.");
        }


    }
}
