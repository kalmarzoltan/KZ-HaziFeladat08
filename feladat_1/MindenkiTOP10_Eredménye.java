package hazi_8.feladat_1;

import java.io.*;
import java.util.*;

public class MindenkiTOP10_Eredménye {

    public void mutatTOP10() {
        String line;
        BufferedReader bfr = null;
        Map<String, Integer> top10 = new HashMap<>();

        try {
            bfr = new BufferedReader(
                    new FileReader("C:\\Users\\Kalmi\\Documents\\gamers.txt"));


            while ((line = bfr.readLine()) != null) {
                String gamerName = (line.split(";")[0]).substring(28);

                String eredmény = line.substring(line.indexOf("próbálkozások száma: ") + 21, line.indexOf(", a gondolt"));
                int szam = Integer.parseInt(eredmény);


                if ((top10.get(gamerName) == null)) {
                    top10.put(gamerName, szam);

                } else {
                    if (top10.get(gamerName) < szam) {

                        top10.put(gamerName, szam);
                    }
                }
            }
            System.out.println("TOP 10 scores: ");
            System.out.println(top10);


            for (int i = 1; i < 11; i++) {
                String topGamer = Collections.max(top10.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey();
                System.out.println("No" + i + " " + topGamer + " : " + top10.get(topGamer));
                top10.remove(topGamer);

            }
            System.out.println();


        } catch (FileNotFoundException e) {
            System.out.println("File not found exception van...");
        } catch (IOException e) {
            System.out.println("IO exception van...");
        } finally {
            try {
                bfr.close();
            } catch (IOException e) {
                System.err.println("Nem lehetett lezárni a file-t");
            }
        }
    }
}
