package hazi_8.feladat_1;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MásikJátékosEredménye {


    public void mutat(String név) {

        String line;
        BufferedReader bfr = null;
        try {
            bfr = new BufferedReader(
                    new FileReader("C:\\Users\\Kalmi\\Documents\\gamers.txt"));

            while ((line = bfr.readLine()) != null) {

                if (név != null) {
                    String gamerName = (line.split(";")[0]).substring(28);
                    if (név.equalsIgnoreCase(gamerName)) {

                        System.out.println(line);
                    }


                } else {
                    System.out.println("Nem adtál meg semmit.");
                }


                // bfr.close(); IDE EZ NEM KELL!!!
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found exception van...");
        } catch (IOException e) {
            System.out.println("IO exception van...");
        } finally {
            try {
                bfr.close();
            } catch (IOException e) {
                System.err.println("Nem lehetett lezárni a file-t");
            }
        }

    }

}
