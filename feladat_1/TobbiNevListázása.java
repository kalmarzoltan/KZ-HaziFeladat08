package hazi_8.feladat_1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TobbiNevListázása {


    public void listáz() {
        String line;
        List<String> gamersListWithDuplicates = new ArrayList<>();
        Set<String> gamersWoDuplicates = new HashSet<>();


        BufferedReader bfr = null;
        try {
            bfr = new BufferedReader(
                    new FileReader("C:\\Users\\Kalmi\\Documents\\gamers.txt"));

            while ((line = bfr.readLine()) != null) {
                String gamerName = (line.split(";")[0]).substring(28);
                gamersListWithDuplicates.add(gamerName);
                // bfr.close(); IDE EZ NEM KELL!!!
            }

            gamersWoDuplicates.addAll(gamersListWithDuplicates);
            gamersListWithDuplicates.clear();
            gamersListWithDuplicates.addAll(gamersWoDuplicates);

            System.out.println("A játékosok nevei (nem duplikálva): "+gamersWoDuplicates.toString());


        } catch (FileNotFoundException e) {
            System.out.println("File not found exception van...");
        } catch (IOException e) {
            System.out.println("IO exception van...");
        } finally {
            try {
                bfr.close();
            } catch (IOException e) {
                System.err.println("Nem lehetett lezárni a file-t");
            }
        }

    }

}
