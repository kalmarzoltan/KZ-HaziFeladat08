package hazi_8.feladat_1;

import java.util.Scanner;
import java.util.InputMismatchException;

public class NumberGuessRunner {

    static int menuPont;
    static Scanner scn = null;


    public static void main(String[] args) {


            while (menuPont != 10) {


        System.out.println("Válassz menüpontot: ");
        System.out.println("\t1: Új felhasználó név megadása.");
        System.out.println("\t2: Új játék indítása.");
        System.out.println("\t3: Saját korábbi eredmények visszanézése.");
        System.out.println("\t4: Sajat Top3 eredményt megnézni.");
        System.out.println("\t5: Sajat korabbi eredmenyeket torolni.");
        System.out.println("\t6: Többi játékos nevének kilistázása.");
        System.out.println("\t7: Játékos adatainak megtekintése.");
        System.out.println("\t8: Játékos TOP3 adatainak megtekintése.");
        System.out.println("\t9: TOP10 -es lista megtekintése.");
        System.out.println("\t10: Kilepes");

        gameRun();

         }
    }

    public static void gameRun() {

        try {
            scn = new Scanner(System.in);
            menuPont = scn.nextInt();

            switch (menuPont) {

                case 1:
                    System.out.println("Új felhasználó név megadása: ");
                    String gamerName = scn.nextLine();
                    Gamer gamer = new Gamer(gamerName,0,0,null);
                    NumberGuess numberGuess2 = new NumberGuess(gamer);
                    numberGuess2.guessTheNumber();
                    break;

                case 2:
                    System.out.println("Új játék indítása..");
                    NumberGuess numberGuess1 = new NumberGuess();
                    numberGuess1.guessTheNumber();

                    break;

                case 3:
                    System.out.println("Saját korábbi eredmények visszanézése.");
                    break;

                case 4:
                    System.out.println("Saját Top3 eredményt megnézni.");
                    break;

                case 5:
                    System.out.println("Saját korábbi eredményeket törölni.");
                    break;

                case 6:
                    System.out.println("Többi játékos nevének kilistázása.");
                    TobbiNevListázása tbl = new TobbiNevListázása();
                    tbl.listáz();
                    break;

                case 7:
                    System.out.println("Játékos adatainak megtekintése");
                    System.out.println("Add meg a nevét:");
                    String név = scn.next();
                    MásikJátékosEredménye mj = new MásikJátékosEredménye();
                    mj.mutat(név);
                    break;

                case 8:
                    System.out.println("Játékos TOP3 adatainak megtekintése.");
                    System.out.println("Add meg a nevét:");
                    String név2 = scn.next();
                    MásikJátékosTOP3_Eredménye top3 = new MásikJátékosTOP3_Eredménye();
                    top3.mutatTOP3(név2);
                    break;

                case 9:
                    System.out.println("TOP10 -es lista megtekintése.");
                    MindenkiTOP10_Eredménye top10 = new MindenkiTOP10_Eredménye();
                    top10.mutatTOP10();
                    break;

                case 10:
                    System.out.println("Kilepes");
                    System.exit(0);
                    break;
            }
        } catch (InputMismatchException ex) {
            System.err.println("Nem számot adtál meg");
        }
    }
}