package hazi_8.feladat_1;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Gamer {


    private String name;
    private int numberOfGames;
    private int numberOfTries;
    private List<Integer> gondoltSzamok;
    Date date = new Date();


    public Gamer(String name, int numberOfGames, int numberOfTries, List<Integer> gondoltSzamok) {

        this.name = name;
        this.numberOfGames = numberOfGames;
        this.numberOfTries = numberOfTries;
        this.gondoltSzamok = gondoltSzamok;
    }





    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfGames() {
        return numberOfGames;
    }

    public void setNumberOfGames(int numberOfGames) {
        this.numberOfGames = numberOfGames;
    }

    public int getNumberOfTries() {
        return numberOfTries;
    }

    public void setNumberOfTries(int numberOfTries) {
        this.numberOfTries = numberOfTries;
    }

    public List<Integer> getGondoltSzamok() {
        return gondoltSzamok;
    }

    public void setGondoltSzamok(List<Integer> gondoltSzamok) {
        this.gondoltSzamok = gondoltSzamok;
    }


    @Override
    public String toString() {
        return
                "Dátum: " + new SimpleDateFormat("yyyy-MM-dd").format(date) +
                ", Játékos: " + name +
                "; játékok száma: " + numberOfGames +
                ", összes próbálkozások száma: " + numberOfTries +
                ", a gondolt szám(ok): " + gondoltSzamok ;
    }
}
