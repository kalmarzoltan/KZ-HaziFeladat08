package hazi_8.feladat_1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MásikJátékosTOP3_Eredménye {


    public void mutatTOP3(String név) {

        String line;
        BufferedReader bfr = null;
        List<Integer> eredményei = new ArrayList<>();

        try {
            bfr = new BufferedReader(
                    new FileReader("C:\\Users\\Kalmi\\Documents\\gamers.txt"));

            while ((line = bfr.readLine()) != null) {

                if (név != null) {

                    String gamerName = (line.split(";")[0]).substring(28);
                    if (név.equalsIgnoreCase(gamerName)) {

                        String eredmény = line.substring(line.indexOf("próbálkozások száma: ") +21, line.indexOf(", a gondolt"));
                        int szam = Integer.parseInt(eredmény);

                        eredményei.add(szam);


                    }


                } else {
                    System.out.println("Nem adtál meg semmit.");
                }


            }


            System.out.println("TOP 3 score: ");
            System.out.println(Collections.max(eredményei));
            eredményei.remove(Collections.max(eredményei));
            System.out.println(Collections.max(eredményei));
            eredményei.remove(Collections.max(eredményei));
            System.out.println(Collections.max(eredményei));


        } catch (FileNotFoundException e) {
            System.out.println("File not found exception van...");
        } catch (IOException e) {
            System.out.println("IO exception van...");
        } finally {
            try {
                bfr.close();
            } catch (IOException e) {
                System.err.println("Nem lehetett lezárni a file-t");
            }
        }

    }

}
