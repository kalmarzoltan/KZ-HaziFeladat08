package hazi_8.feladat_2;

import java.io.*;
import java.util.Scanner;

public class FileCheckerGame {
    static File file = null;
    static Scanner scn = new Scanner(System.in);
    static String eleresiUt = null;
    static int menuPont;

    public static void main(String[] argrs) {
        System.out.println("Kérem az utasitast: ");
        while (menuPont != 1) {
            checkTheFile();
        }
    }

    public static void checkTheFile() {
        System.out.println("\t1: Kilepes");
        System.out.println("\t2: új file megadása");
        int menuPont = scn.nextInt();

        switch (menuPont) {
            case 1:
                System.out.println("Kilepes");
                System.exit(0);
                break;
            case 2:
                System.out.println("új file megadása, kérlek gépeld be az elérési utat");
                eleresiUt = scn.next();
                System.out.println("A megadott elérési út:" + eleresiUt);
                file = new File(eleresiUt);
                break;

        }
        while (menuPont != 1) {

        System.out.println("Kérem az utasitast: ");
        System.out.println("\t1: Kilepes");
        System.out.println("\t2: Tartalom kiírása");
        System.out.println("\t3: File adatok kiírása");
        menuPont = scn.nextInt();


        switch (menuPont) {
            case 1:
                System.out.println("Kilepes");
                System.exit(0);
                break;
            case 2:
                System.out.println("A tartalom:");
                String line;
                try (BufferedReader bfr = new BufferedReader(new FileReader(eleresiUt))) {

                    while ((line = bfr.readLine()) != null) {
                        System.out.println(line);
                    }
                } catch (FileNotFoundException e) {
                    System.out.println("File not found exception van...");
                } catch (IOException e) {
                    System.out.println("IO exception van...");
                }
                break;
            case 3:
                System.out.println("File adatok:");
                System.out.println("Létezik-e: " + file.exists() + "; mérete: " + file.getTotalSpace() + "; path: "
                        + file.getAbsolutePath());
                break;

        }
    }
    }
}
